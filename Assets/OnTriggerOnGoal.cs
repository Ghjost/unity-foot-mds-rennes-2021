﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerOnGoal : MonoBehaviour
{
    public GameObject Monprefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collider){
        if (collider.gameObject.CompareTag("Ball"))
        {
            // Debug.Log("TRIIIIGGGGGEEEEER");
            // Destroy(collider.gameObject);
            // var instance = Instantiate(Monprefab);
            // instance.transform.position = Vector3.zero;
            if (collider.gameObject.CompareTag("GoalLeft"))
            {
                Debug.Log("Trigger left");
                Destroy(collider.gameObject);
                var instance = Instantiate(Monprefab);
                instance.transform.position = Vector3.zero;
            }
            if (collider.gameObject.CompareTag("GoalRight"))
            {
                Debug.Log("Trigger Right");
                Destroy(collider.gameObject);
                var instance = Instantiate(Monprefab);
                instance.transform.position = Vector3.zero;
            }
        }

        

    }
}
