﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedKnight : MonoBehaviour
{
    Animator animator;
    SpriteRenderer spriteRenderer;
    Transform transform;
    Rigidbody2D rigidbody2d;

    public int MaxSpeed = 10;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        transform = GetComponent<Transform>();
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    void FixedUpdate()
    {
        // var maxDistancePerFrame = MaxSpeed;
        // Vector3 move = Vector3.zero;
        // rigidbody2d.velocity = Vector2.zero;
        var Myvelo = Vector2.zero;


        if (Input.GetKey(KeyCode.RightArrow)) {
            animator.SetFloat("Speed", 2f);
            spriteRenderer.flipX = false;
            // this.transform.position -= Vector3.left * MaxSpeed * Time.deltaTime;
            Myvelo = new Vector2(MaxSpeed, 0);

        }else if(Input.GetKey(KeyCode.LeftArrow)){
            animator.SetFloat("Speed", 2f);
            spriteRenderer.flipX = true;
            Myvelo = new Vector2(-MaxSpeed, 0);
            // this.transform.position -= Vector3.right * MaxSpeed * Time.deltaTime;

        }else if(Input.GetKey(KeyCode.UpArrow)){
            animator.SetFloat("Speed", 2f);
            spriteRenderer.flipX = false;
            Myvelo = new Vector2(0, MaxSpeed);

            // this.transform.position += Vector3.up * MaxSpeed * Time.deltaTime;

        }else if(Input.GetKey(KeyCode.DownArrow)){
            animator.SetFloat("Speed", 2f);
            spriteRenderer.flipX = false;
            Myvelo = new Vector2(0, -MaxSpeed);
            // this.transform.position += Vector3.down * MaxSpeed * Time.deltaTime;

        }else {
            animator.SetFloat("Speed", 0f);
            animator.SetFloat("Roll", 0f);
        }

        rigidbody2d.velocity = Myvelo;


        if(Input.GetKeyDown(KeyCode.Space)){
            animator.SetFloat("Roll", 2f);
            spriteRenderer.flipX = false;
        }
    }
}
